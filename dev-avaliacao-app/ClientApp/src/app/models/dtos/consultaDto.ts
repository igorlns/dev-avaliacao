import { Medico } from "../medico";
import { Paciente } from "../paciente";

export class ConsultaDto {
    id: number;
    data: Date;
    endereco: string;
    medico: Medico = null;
    paciente: Paciente = null;
    observacao: string;
    constructor() {
    }
}
