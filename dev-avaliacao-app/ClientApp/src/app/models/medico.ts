export class Medico {
    id: number;
    nome: string;
    cpf: string;
    dataNascimento: Date;
    idade: number;
    genero: any = null;
    crm: string;
    especialidade: string = null;
    constructor() { }
}
