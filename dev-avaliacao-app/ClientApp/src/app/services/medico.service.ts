import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Medico } from '../models/medico';
import { ConsultaDto } from '../models/dtos/consultaDto';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {
private apiUrl = environment.apiUrl;
constructor(private http: HttpClient) { }

getMedicos(): Observable<Medico[]> {
  return this.http.get<Medico[]>(`${this.apiUrl}/medicos`);
}

getAgenda(agenda: any): Observable<ConsultaDto[]> {
  return this.http.post<ConsultaDto[]>(`${this.apiUrl}/medicos/agenda`, agenda);
}

getEspecialidades(): Observable<any[]> {
  return this.http.get<any[]>(`${this.apiUrl}/medicos/especialidades`);
}

getMedico(id: number): Observable<Medico> {
  return this.http.get<Medico>(`${this.apiUrl}/medicos/${id}`);
}

postMedico(medico: Medico) {
  return this.http.post<Medico>(`${this.apiUrl}/medicos/`, medico);
}

putMedico(medico: Medico) {
  return this.http.put<Medico>(`${this.apiUrl}/medicos/${medico.id}`, medico);
}

deleteMedico(Id: number) {
  return this.http.delete(`${this.apiUrl}/medicos/${Id}`);
}

}
