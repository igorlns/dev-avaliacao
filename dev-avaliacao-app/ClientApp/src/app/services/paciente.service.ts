import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Paciente } from '../models/paciente';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getPacientes(): Observable<Paciente[]> {
    return this.http.get<Paciente[]>(`${this.apiUrl}/pacientes`);
  }

  getPaciente(id: number): Observable<Paciente> {
    return this.http.get<Paciente>(`${this.apiUrl}/pacientes/${id}`);
  }

  postPaciente(paciente: Paciente) {
    return this.http.post<Paciente>(`${this.apiUrl}/pacientes/`, paciente);
  }

  putPaciente(paciente: Paciente) {
    return this.http.put<Paciente>(`${this.apiUrl}/pacientes/${paciente.id}`, paciente);
  }

  deletePaciente(Id: number) {
    return this.http.delete(`${this.apiUrl}/pacientes/${Id}`);
  }

}
