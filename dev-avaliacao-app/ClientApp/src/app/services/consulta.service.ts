import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConsultaDto } from '../models/dtos/consultaDto';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {

  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getConsultas(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiUrl}/consultas`);
  }

  getConsulta(id: number): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/consultas/${id}`);
  }

  postConsulta(consultaDto: ConsultaDto) {
    return this.http.post<any>(`${this.apiUrl}/consultas/`, consultaDto);
  }

  putConsulta(consultaDto: ConsultaDto) {
    return this.http.put<any>(`${this.apiUrl}/consultas/${consultaDto.id}`, consultaDto);
  }

  deleteConsulta(Id: number) {
    return this.http.delete(`${this.apiUrl}/consultas/${Id}`);
  }

}