import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { HomeComponent } from './pages/home/home.component';
import { MedicosModule } from './pages/medicos/medicos.module';
import { MedicosComponent } from './pages/medicos/medicos.component';
import { AgendaComponent } from './pages/medicos/agenda/agenda.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PacientesComponent } from './pages/pacientes/pacientes.component';
import { PacientesModule } from './pages/pacientes/pacientes.module';
import { ConsultasComponent } from './pages/consultas/consultas.component';
import { ConsultasModule } from './pages/consultas/consultas.module';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    NavMenuComponent,
  ],
  imports: [
    MedicosModule,
    PacientesModule,
    ConsultasModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'medicos', component: MedicosComponent},
      { path: 'agenda', component: AgendaComponent },
      { path: 'pacientes', component: PacientesComponent },
      { path: 'consultas', component: ConsultasComponent }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }