import { Component, OnInit } from '@angular/core';
import { Paciente } from 'src/app/models/paciente';
import { GeneroPessoa } from 'src/app/models/generoPessoa';
import { PacienteService } from 'src/app/services/paciente.service';
import { NoticicationService } from 'src/app/services/noticication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.scss']
})
export class PacientesComponent implements OnInit {
  registerForm: FormGroup;
  pacientes: Paciente[];
  generos: GeneroPessoa[];

  itemSelecionado: { isChecked: boolean, qtd: number };
  paciente: Paciente;

  constructor(private pacienteService: PacienteService, private notify: NoticicationService,
    private fb: FormBuilder) {
    this.paciente = new Paciente();
    this.pacientes = new Array<Paciente>();
   }

  ngOnInit() {
    this.validation();
    this.loadPacientes();
    this.loadGeneroPessoas();
  }

  loadPacientes() {
    this.pacienteService.getPacientes().subscribe(res => {
      this.pacientes = res;
    }, error => {
      console.log(error);
    });
  }

  loadGeneroPessoas() {
    this.generos = [new GeneroPessoa(0, 'Masculino'), new GeneroPessoa(1, 'Feminino')];
  }

  onChange(paciente: Paciente, isChecked: boolean) {
   let qtd = document.querySelectorAll('input[name=checkboxes]:checked').length;
   this.paciente = isChecked ? paciente : new Paciente();
   this.itemSelecionado = { isChecked, qtd };
   if (qtd > 1) {
     this.notify.showWarning('Você deve selecionar apenas um registro!', 'Atenção');
   }
  }

  validation() {
    this.registerForm = this.fb.group({
      nome: ['', [Validators.required]],
      cpf: ['', Validators.required],
      dataNascimento: ['', Validators.required],
      genero: [null, [Validators.required]],
    });
  }
  salvarPaciente() {
    if (this.paciente.id > 0) {
      this.atualizarPaciente();
    } else {
      this.paciente = Object.assign({}, this.registerForm.value);
      this.salvarNovoPaciente();
    }
  }

  salvarNovoPaciente() {
    this.pacienteService.postPaciente(this.paciente).subscribe(res => {
      let element: HTMLElement = document.getElementById('btn-sair') as HTMLElement;
      element.click();
      this.notify.showSuccess('Novo paciente cadastrado com sucesso', 'OK!');
      this.loadPacientes();
    },
      error => {
        this.notify.showError('Erro ao tentar criar novo paciente!', 'Ops!');
      });
  }

  atualizarPaciente() {
    this.pacienteService.putPaciente(this.paciente).subscribe(res => {
      let element: HTMLElement = document.getElementById('btn-sair') as HTMLElement;
      element.click();
      this.notify.showSuccess('Paciente atualizado com sucesso', 'OK!');
    },
      error => {
        this.notify.showError('Erro ao tentar atualizar paciente!', 'Ops!');
      });
  }

  apagarPaciente() {
    this.pacienteService.deletePaciente(this.paciente.id).subscribe(res => {
      this.notify.showSuccess('Paciente excluído com sucesso', 'OK!');
      this.loadPacientes();
    },
      error => {
        this.notify.showError('Erro ao tentar excluir paciente!', 'Ops!');
      });
  }

}
