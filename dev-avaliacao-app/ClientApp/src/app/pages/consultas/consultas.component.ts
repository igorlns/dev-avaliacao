import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Paciente } from 'src/app/models/paciente';
import { Medico } from 'src/app/models/medico';
import { ConsultaDto } from 'src/app/models/dtos/consultaDto';
import { ConsultaService } from 'src/app/services/consulta.service';
import { NoticicationService } from 'src/app/services/noticication.service';
import { PacienteService } from 'src/app/services/paciente.service';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-consultas',
  templateUrl: './consultas.component.html',
  styleUrls: ['./consultas.component.scss']
})
export class ConsultasComponent implements OnInit {

  registerForm: FormGroup;
  pacientes: Paciente[];
  medicos: Medico[];
  consultas: ConsultaDto[];

  itemSelecionado: { isChecked: boolean, qtd: number };
  consultaDto: ConsultaDto;

  showSpinner: Boolean;

  constructor(private consultaService: ConsultaService, private notify: NoticicationService,
    private fb: FormBuilder, private pacienteService: PacienteService, private medicoService: MedicoService) {
    this.consultaDto = new ConsultaDto();
    this.pacientes = new Array<Paciente>();
    this.medicos = new Array<Medico>();
    this.consultas = new Array<ConsultaDto>();

   }

  ngOnInit() {
    this.validation();
    this.loadPacientes();
    this.loadMedicos();
    this.loadConsultas();
  }

  loadPacientes() {
    this.pacienteService.getPacientes().subscribe(res => {
      this.pacientes = res;
    }, error => {
      console.log(error);
    });
  }

  loadMedicos() {
    this.medicoService.getMedicos().subscribe(res => {
      this.medicos = res;
      this.medicos.push(new Medico());
    }, error => {
      console.log(error);
    });
  }

  loadConsultas() {
    this.showSpinner = true;
    this.consultaService.getConsultas().subscribe(res => {
      this.showSpinner = false;
      this.consultas = res;
    }, error => {
      this.showSpinner = false;
      console.log(error);
    });
  }

  onChange(consultaDto: ConsultaDto, isChecked: boolean) {
   let qtd = document.querySelectorAll('input[name=checkboxes]:checked').length;
   this.consultaDto = isChecked ? consultaDto : new ConsultaDto();
   this.itemSelecionado = { isChecked, qtd };
   if (qtd > 1) {
     this.notify.showWarning('Você deve selecionar apenas um registro!', 'Atenção');
   }
  }

  validation() {
    this.registerForm = this.fb.group({
      data: ['', Validators.required],
      endereco: ['', Validators.required],
      observacao: ['', Validators.required],
      medico: [null, Validators.required],
      paciente: [null, Validators.required]
    });
  }
  salvarConsulta() {
    if (this.consultaDto.id > 0) {
      this.atualizarConsulta();
    } else {
      this.consultaDto = Object.assign({}, this.registerForm.value);
      this.salvarNovaConsulta();
    }
  }

  salvarNovaConsulta() {
    this.consultaService.postConsulta(this.consultaDto).subscribe(res => {
      let element: HTMLElement = document.getElementById('btn-sair') as HTMLElement;
      element.click();
      this.notify.showSuccess('Nova consulta cadastrada com sucesso', 'OK!');
      this.loadConsultas();
    },
      error => {
        this.notify.showError('Erro ao tentar criar nova consulta!', 'Ops!');
      });
  }

  atualizarConsulta() {
    this.consultaService.putConsulta(this.consultaDto).subscribe(res => {
      let element: HTMLElement = document.getElementById('btn-sair') as HTMLElement;
      element.click();
      this.notify.showSuccess('Consulta atualizada com sucesso', 'OK!');
    },
      error => {
        this.notify.showError('Erro ao tentar atualizar consulta!', 'Ops!');
      });
  }

  apagarConsulta() {
    this.consultaService.deleteConsulta(this.consultaDto.id).subscribe(res => {
      this.notify.showSuccess('Consulta excluída com sucesso', 'OK!');
      this.loadConsultas();
    },
      error => {
        this.notify.showError('Erro ao tentar excluir consulta!', 'Ops!');
      });
  }

}
