import { Component, OnInit } from '@angular/core';
import { Medico } from 'src/app/models/medico';
import { GeneroPessoa } from 'src/app/models/generoPessoa';
import { EpecialidadeMedica } from 'src/app/models/epecialidadeMedica';
import { MedicoService } from 'src/app/services/medico.service';
import { NoticicationService } from 'src/app/services/noticication.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-medicos',
  templateUrl: './medicos.component.html',
  styleUrls: ['./medicos.component.scss']
})
export class MedicosComponent implements OnInit {
  registerForm: FormGroup;
  medicos: Medico[];
  generos: GeneroPessoa[];
  especialidades: EpecialidadeMedica[];


  itemSelecionado: { isChecked: boolean, qtd: number };
  medico: Medico;

  constructor(private medicoService: MedicoService, private notify: NoticicationService, private fb: FormBuilder) {
    this.medico = new Medico();
    this.medicos = new Array<Medico>();
   }

  ngOnInit() {
    this.validation();
    this.loadMedicos();
    this.loadGeneroPessoas();
    this.loadEspecialidades();
  }

  loadMedicos() {
    this.medicoService.getMedicos().subscribe(res => {
      this.medicos = res;
    }, error => {
      console.log(error);
    });
  }

  loadGeneroPessoas() {
    this.generos = [new GeneroPessoa(0, 'Masculino'), new GeneroPessoa(1, 'Feminino')];
  }

  loadEspecialidades() {
    this.medicoService.getEspecialidades().subscribe(res => {
      this.especialidades = res;
    },
      error => {
        console.log(error);
      });
  }

  onChange(medico: Medico, isChecked: boolean) {
   let qtd = document.querySelectorAll('input[name=checkboxes]:checked').length;
   this.medico = isChecked ? medico : new Medico();
   this.itemSelecionado = { isChecked, qtd };
   if (qtd > 1) {
     this.notify.showWarning('Você deve selecionar apenas um registro!','Atenção');
   }
  }

  validation() {
    this.registerForm = this.fb.group({
      nome: ['', [Validators.required]],
      cpf: ['', Validators.required],
      crm: ['', Validators.required],
      dataNascimento: ['', Validators.required],
      genero: [null, [Validators.required]],
      especialidade: [null, [Validators.required]],
    });
  }

  salvarMedico() {
    if (this.medico.id > 0) {
      this.atualizarMedico();
    } else {
      this.medico = Object.assign({}, this.registerForm.value);
      this.salvarNovoMedico();
    }
  }

  salvarNovoMedico() {
    this.medicoService.postMedico(this.medico).subscribe(res => {
      let element: HTMLElement = document.getElementById('btn-sair') as HTMLElement;
      element.click();
      this.notify.showSuccess('Novo médico cadastrado com sucesso', 'OK!');
      this.loadMedicos();
    },
      error => {
        this.notify.showError('Erro ao tentar criar novo médico!', 'Ops!');
      });
  }

  atualizarMedico() {
    this.medicoService.putMedico(this.medico).subscribe(res => {
      let element: HTMLElement = document.getElementById('btn-sair') as HTMLElement;
      element.click();
      this.notify.showSuccess('Médico atualizado com sucesso', 'OK!');
    },
      error => {
        this.notify.showError('Erro ao tentar atualizar médico!', 'Ops!');
      });
  }

  apagarMedico() {
    this.medicoService.deleteMedico(this.medico.id).subscribe(res => {
      this.notify.showSuccess('Médico excluído com sucesso', 'OK!');
      this.loadMedicos();
    },
      error => {
        this.notify.showError('Erro ao tentar excluir médico!', 'Ops!');
      });
  }
}