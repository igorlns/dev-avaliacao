import { Component, OnInit } from '@angular/core';
import { Medico } from 'src/app/models/medico';
import { MedicoService } from 'src/app/services/medico.service';
import { ConsultaService } from 'src/app/services/consulta.service';
import { ConsultaDto } from 'src/app/models/dtos/consultaDto';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit {
  medicos: Medico[];
  consultas: ConsultaDto[];
  consultasFiltradas: ConsultaDto[];
  medico: Medico;
  data: Date;
  showSpinner: boolean;
  constructor(private medicoService: MedicoService, private consultaService: ConsultaService) { 
    this.consultas = new Array<ConsultaDto>();
    this.consultasFiltradas = new Array<ConsultaDto>();
  }

  ngOnInit() {
    this.loadMedicos();
    this.loadConsultas();
  }

  loadMedicos() {
    this.medicoService.getMedicos().subscribe(res => {
      this.medicos = res;
    }, error => {
      console.log(error);
    });
  }

  loadConsultas() {
    this.consultaService.getConsultas().subscribe(res => {
      this.consultas = res;
    }, error => {
      console.log(error);
    });

  }

  filtrar() {
    if (this.medico) this.consultasFiltradas = this.consultas.filter(c => c.medico.id === this.medico.id);

    if (this.data) {
      this.showSpinner = true;
      let agenda = new AgendaDTO(this.data, this.medico);
      this.medicoService.getAgenda(agenda).subscribe((res: ConsultaDto[]) => {
        this.showSpinner = false;
        console.log(res);
        this.consultasFiltradas = res;
      });
      }
  }

}

class AgendaDTO {
  data: Date;
  medico: Medico;
  constructor(data, medico) {
    this.data = data;
    this.medico = medico;
  }
}